package net.mizofumi.llne2.View.Activity.Local.Original;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.baasbox.android.json.JsonObject;

import net.mizofumi.llne2.BaasBox.API.BaasBoxAPI;
import net.mizofumi.llne2.BaasBox.Bean.Original.OriginGroupBean;
import net.mizofumi.llne2.R;
import net.mizofumi.llne2.SharedPref;
import net.mizofumi.llne2.View.Activity.Local.LlNE2.StampSendActivity;
import net.mizofumi.llne2.View.Dialog.NewGroupDialog;

import java.io.FileDescriptor;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class OriginalUploadActivity extends AppCompatActivity implements NewGroupDialog.DialogCloseListener {
    private static final int NOTIFICATION_SENDING_ID = 1002;
    private static final int NOTIFICATION_ERROR_ID = 1003;
    private static final int RESULT_PICK_IMAGEFILE = 1001;
    private List<String> groupList = new ArrayList<>();
    private List<OriginGroupBean> originGroupBeanList = new ArrayList<>();
    private ArrayAdapter<String> adapter;
    ImageView stamp;
    Button imageSelect;
    Bitmap bitmap;
    Spinner spinner;
    EditText title;
    String groupName;
    String groupId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_original_upload);

        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,groupList);
        spinner = (Spinner)findViewById(R.id.spinner);
        spinner.setAdapter(adapter);
        title = (EditText)findViewById(R.id.title);
        stamp = (ImageView)findViewById(R.id.stamp);
        imageSelect = (Button)findViewById(R.id.imageSelect);
        imageSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image/*");
                startActivityForResult(intent,RESULT_PICK_IMAGEFILE);
            }
        });
        groupName = getIntent().getStringExtra("groupName");
        groupId = getIntent().getStringExtra("groupId");

        (findViewById(R.id.newGroup)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewGroupDialog dialog = NewGroupDialog.newInstance();
                dialog.show(getSupportFragmentManager(),"newGroup");
            }
        });

        (findViewById(R.id.send)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bitmap == null){
                    Toast.makeText(OriginalUploadActivity.this, "画像が設定されていません", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (title.getText().toString().isEmpty()){
                    Toast.makeText(OriginalUploadActivity.this, "タイトルが入力されていません", Toast.LENGTH_SHORT).show();
                    return;
                }
                if ((groupId == null || (groupId.isEmpty()))){
                    Toast.makeText(OriginalUploadActivity.this, "グループが設定されていません", Toast.LENGTH_SHORT).show();
                    return;
                }

                NotificationCompat.Builder builder = new NotificationCompat.Builder(OriginalUploadActivity.this);
                builder.setSmallIcon(R.drawable.ic_send_white_24dp);
                builder.setContentTitle("みぞLlNEサーバ情報");
                builder.setContentText("送信しています");
                builder.setSubText("Please wait…");

                final NotificationManagerCompat managerCompat = NotificationManagerCompat.from(OriginalUploadActivity.this);
                managerCompat.notify(NOTIFICATION_SENDING_ID,builder.build());

                JsonObject attachedData = new JsonObject()
                        .put("userId", SharedPref.loadUserId(OriginalUploadActivity.this))
                        .put("stampName",title.getText().toString())
//                        .put("groupId",originGroupBeanList.get(spinner.getSelectedItemPosition()).getId());
                        .put("groupId",groupId);
                new BaasBoxAPI().sendOriginFile(attachedData, groupId, bitmap, new BaasBoxAPI.SendOriginFileResultHandler() {
                    @Override
                    public void success() {
                        managerCompat.cancel(NOTIFICATION_SENDING_ID);
                        Toast.makeText(OriginalUploadActivity.this, "登録しました", Toast.LENGTH_SHORT).show();
                        finish();
                    }

                    @Override
                    public void failed(String errorMessage) {
                        managerCompat.cancel(NOTIFICATION_SENDING_ID);
                        NotificationCompat.Builder builder = new NotificationCompat.Builder(OriginalUploadActivity.this);
                        builder.setContentTitle("みぞLlNEサーバ結果情報");
                        builder.setSubText("送信or登録に失敗しました");
                        builder.setSubText("Sorry");
//                        managerCompat.notify(NOTIFICATION_ERROR_ID,builder.build());
                        Toast.makeText(OriginalUploadActivity.this, "登録できませんでした\n"+errorMessage, Toast.LENGTH_SHORT).show();
                    }
                });

//                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//                bitmap.compress(Bitmap.CompressFormat.PNG,100,byteArrayOutputStream);
//                final byte[] bytes = byteArrayOutputStream.toByteArray();
//                final BaasFile file = new BaasFile(attachedData);
//                new Thread(new Runnable() {
//                    @Override
//                    public void run() {
//                        file.upload(bytes, new BaasHandler<BaasFile>() {
//                            @Override
//                            public void handle(final BaasResult<BaasFile> imageResult) {
//                                if (imageResult.isSuccess()){
//                                    BaasQuery paginate = BaasQuery.builder().collection("OriginGroups").where("id = ?").whereParams(originGroupBeanList.get(spinner.getSelectedItemPosition()).getId()).build();
//                                    paginate.query(new BaasHandler<List<JsonObject>>() {
//                                        @Override
//                                        public void handle(BaasResult<List<JsonObject>> baasResult) {
//                                            Log.d("orig", String.valueOf(baasResult.value().size()));
//                                            List<JsonObject> list = baasResult.value();
//                                            for (JsonObject j : list) {
//                                                j.put("topImageId",imageResult.value().getId());
//                                                j.put("topImageUri",imageResult.value().getStreamUri().toString());
//                                                BaasDocument document = BaasDocument.from(j);
//                                                document.save(new BaasHandler<BaasDocument>() {
//                                                    @Override
//                                                    public void handle(BaasResult<BaasDocument> baasResult) {
//                                                        new Handler(Looper.getMainLooper()).post(new Runnable() {
//                                                            @Override
//                                                            public void run() {
//                                                                if (baasResult.isSuccess()){
//                                                                    Toast.makeText(OriginalUploadActivity.this, "登録しました", Toast.LENGTH_SHORT).show();
//                                                                    finish();
//                                                                }else {
//
//                                                                }
//                                                            }
//                                                        });
//                                                    }
//                                                });
//                                            }
//                                        }
//                                    });
//                                }else {
//                                    new Handler(Looper.getMainLooper()).post(new Runnable() {
//                                        @Override
//                                        public void run() {
//                                            Toast.makeText(OriginalUploadActivity.this, "登録できませんでした", Toast.LENGTH_SHORT).show();
//                                        }
//                                    });
//                                }
//                            }
//                        });
//                    }
//                }).start();

            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("OriginalUploadActivity", "onResume: ");
        groupList.clear();
        originGroupBeanList.clear();
        adapter.notifyDataSetChanged();
        new BaasBoxAPI().getOriginGroups(SharedPref.loadUserId(this), true, new BaasBoxAPI.OriginGroupListReciveListener() {
            @Override
            public void onListen(List<OriginGroupBean> list) {
                for (OriginGroupBean originGroup:list) {
                    originGroupBeanList.add(originGroup);
                    groupList.add(originGroup.getGroupName());
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onError() {

            }
        });
        new BaasBoxAPI().getOriginGroups(SharedPref.loadUserId(this), false, new BaasBoxAPI.OriginGroupListReciveListener() {
            @Override
            public void onListen(List<OriginGroupBean> list) {
                for (OriginGroupBean originGroup:list) {
                    originGroupBeanList.add(originGroup);
                    groupList.add(originGroup.getGroupName());
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onError() {

            }
        });
    }

    @Override
    public void handleDialogClose(DialogInterface dialogInterface) {
        this.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_PICK_IMAGEFILE && resultCode == Activity.RESULT_OK) {
            Uri uri = null;
            if (data != null) {
                uri = data.getData();
                Log.i("", "Uri: " + uri.toString());
                imageSelect.setVisibility(View.INVISIBLE);
//                spinner.setVisibility(View.VISIBLE);
//                (findViewById(R.id.newGroup)).setVisibility(View.VISIBLE);
                title.setVisibility(View.VISIBLE);
                (findViewById(R.id.send)).setVisibility(View.VISIBLE);
                try {
                    Bitmap bmp = getBitmapFromUri(uri);
                    bitmap = bmp;
                    stamp.setImageBitmap(bmp);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =
                getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }
}
