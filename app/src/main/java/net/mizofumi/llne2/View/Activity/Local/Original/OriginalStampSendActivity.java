package net.mizofumi.llne2.View.Activity.Local.Original;

import android.Manifest;
import android.app.Notification;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import net.mizofumi.llne2.R;
import net.mizofumi.llne2.Slack.API.SlackChannel;
import net.mizofumi.llne2.Slack.API.SlackFilePost;
import net.mizofumi.llne2.Slack.SlackChannelBean;
import net.mizofumi.llne2.TempFileUtil;

import java.io.ByteArrayOutputStream;
import java.util.List;


public class OriginalStampSendActivity extends AppCompatActivity {

    private final static int NOTIFICATION = 1;
    Bitmap bitmap;
    ListView listView;
    ImageView imageView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_original_stamp_send);
        imageView = (ImageView)findViewById(R.id.imageView);
        listView = (ListView) findViewById(R.id.listView);

        Picasso.with(this).load(getIntent().getStringExtra("imageUri")).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                OriginalStampSendActivity.this.bitmap = bitmap;
                imageView.setImageBitmap(bitmap);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });
    }

    public void sendSlack(String name, String channelId){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100,byteArrayOutputStream);
        final byte[] bytes = byteArrayOutputStream.toByteArray();

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setSmallIcon(R.drawable.ic_file_upload_white_24dp);
        builder.setContentTitle("Slack送信中");
        builder.setContentText("LlNE2");
        builder.setSubText("Please wait…");

        final NotificationManagerCompat manager = NotificationManagerCompat.from(this);
        manager.notify(NOTIFICATION,builder.build());


        new SlackFilePost().sendStamp(this, name, bytes, channelId, new SlackFilePost.SendStampResultHandler() {
            @Override
            public void success() {
                manager.cancel(NOTIFICATION);
            }

            @Override
            public void failed() {
                Toast.makeText(OriginalStampSendActivity.this, "送信に失敗しました", Toast.LENGTH_SHORT).show();
                manager.cancel(NOTIFICATION);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        new SlackChannel().getChannels(this, new SlackChannel.SlackChannelListListener() {
            @Override
            public void success(final List<SlackChannelBean> list) {
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(OriginalStampSendActivity.this, android.R.layout.simple_list_item_1);
                for (SlackChannelBean channel : list) {
                    adapter.add(channel.getName());
                }
                listView.setAdapter(adapter);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String name = list.get(position).getName();
                        String channelId = list.get(position).getId();
                        sendSlack(name,channelId);
                        finish();
                    }
                });
            }

            @Override
            public void error() {

            }
        });
    }

}
