package net.mizofumi.llne2.BaasBox.Bean.Original;

/**
 * Created by mizofumi on 2017/06/17.
 */

public class OriginalStampBean {
    private String id;
    private String userId;
    private String name;
    private String groupId;
    private String imageUri;

    public OriginalStampBean(String id, String userId, String name, String groupId, String imageUri) {
        this.id = id;
        this.userId = userId;
        this.name = name;
        this.groupId = groupId;
        this.imageUri = imageUri;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getImageUri() {
        return imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }
}
