package net.mizofumi.llne2.View.Adapter.Local.LlNE2;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import net.mizofumi.llne2.BaasBox.Bean.LlNE2.StampBean;
import net.mizofumi.llne2.R;
import net.mizofumi.llne2.View.Activity.Local.LlNE2.StampSendActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mizofumi on 2017/06/10.
 */

public class StampAdapter extends BaseAdapter{
    private ViewHolder viewHolder;
    private List<StampBean> list = new ArrayList<>();
    private Context context;
    private LayoutInflater inflater;

    public StampAdapter(Context context) {
        this.context = context;
        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public StampBean getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void add(StampBean stampBean){
        list.add(stampBean);
        notifyDataSetChanged();
    }

    public void add(int position,StampBean stampBean){
        list.add(position, stampBean);
        notifyDataSetChanged();
    }

    public void remove(int position){
        list.remove(position);
        notifyDataSetChanged();
    }

    public void clear(){
        list.clear();
        notifyDataSetChanged();
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null){
            convertView = inflater.inflate(R.layout.stamp_row,parent,false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        Picasso.with(context).load(list.get(position).getImageUrl()).into(viewHolder.stampImageView);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, StampSendActivity.class);
                intent.putExtra("imageUrl",list.get(position).getImageUrl());
                intent.putExtra("sourceUrl",list.get(position).getImage());
                context.startActivity(intent);
            }
        });

        return convertView;
    }

    private class ViewHolder {
        ImageView stampImageView;
        public ViewHolder(View view) {
            stampImageView = (ImageView)view.findViewById(R.id.stampImageView);
        }
    }
}
