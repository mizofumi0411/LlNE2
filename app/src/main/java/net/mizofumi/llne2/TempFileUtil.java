package net.mizofumi.llne2;

import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by mizofumi on 2017/06/17.
 */

public class TempFileUtil {

    public static void saveFile(Bitmap bitmap){
        try {
            // sdcardフォルダを指定
            File root = Environment.getExternalStorageDirectory();
            // 保存処理開始
            FileOutputStream fos = null;
            fos = new FileOutputStream(new File(root, "llne" + ".png"));
            // jpegで保存
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            // 保存処理終了
            fos.close();
        } catch (Exception e) {
            Log.e("Error", "" + e.toString());
        }
    }

    public static File readFile(){
        return new File(Environment.getExternalStorageDirectory(),"llne" + ".png");
    }
}
