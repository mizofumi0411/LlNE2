package net.mizofumi.llne2.View.Activity.Local;

import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.baasbox.android.BaasHandler;
import com.baasbox.android.BaasResult;
import com.baasbox.android.BaasUser;

import net.mizofumi.llne2.R;
import net.mizofumi.llne2.SharedPref;

public class StartupActivity extends AppCompatActivity {

    ImageView success;
    ProgressBar progressBar;
    TextView message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startup);

        success = (ImageView)findViewById(R.id.success);
        success.setVisibility(View.INVISIBLE);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        message = (TextView)findViewById(R.id.message);
        message.setText("認証中…");

        if (SharedPref.loadAccessToken(this) == null || SharedPref.loadUserId(this) == null){
            startActivity(new Intent(this,OAuthActivity.class));
            finish();
        }else {

            BaasUser baasUser = BaasUser.withUserName("admin").setPassword("admin");
            baasUser.login(new BaasHandler<BaasUser>() {
                @Override
                public void handle(BaasResult<BaasUser> baasResult) {
                    if (baasResult.isSuccess()){
                        progressBar.setVisibility(View.INVISIBLE);
                        success.setVisibility(View.VISIBLE);
                        message.setText("認証完了!");
                        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                startActivity(new Intent(StartupActivity.this,MainActivity.class));
                                finish();
                            }
                        },200);
                    }else if (baasResult.isFailed()){
                        success.setImageResource(R.drawable.sad);
                        success.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.INVISIBLE);
                        message.setText("認証失敗…");
                        Toast.makeText(StartupActivity.this, baasResult.error().toString(), Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }
}
