package net.mizofumi.llne2.View.Activity.Local;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.GridView;

import net.mizofumi.llne2.BaasBox.API.BaasBoxAPI;
import net.mizofumi.llne2.BaasBox.Bean.LlNE2.StickerBean;
import net.mizofumi.llne2.R;
import net.mizofumi.llne2.SharedPref;
import net.mizofumi.llne2.View.Activity.LINE2.LlNE2StoreActivity;
import net.mizofumi.llne2.View.Adapter.Local.LlNE2.StickersAdapter;

import java.util.List;

public class OldMainActivity extends AppCompatActivity {

    GridView gridView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oldmain);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(OldMainActivity.this,LlNE2StoreActivity.class));
            }
        });


        gridView = ((GridView)findViewById(R.id.stickers));
    }

    @Override
    protected void onResume() {
        super.onResume();
        final StickersAdapter adapter = new StickersAdapter(this);
        gridView.setAdapter(adapter);
        new BaasBoxAPI().getStickers(SharedPref.loadUserId(this), new BaasBoxAPI.StickerListReciveListener() {
            @Override
            public void onListen(List<StickerBean> list) {
                for (StickerBean stickerBean : list) {
                    adapter.add(stickerBean);
                }
            }

            @Override
            public void onError() {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
