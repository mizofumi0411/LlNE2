package net.mizofumi.llne2.View.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import net.mizofumi.llne2.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class CrazyFontFragment extends Fragment {

    String[] fonts = new String[]{"バジリスクタイム","ドカベン","けものフレンズ"};
    Spinner spinner;

    public static CrazyFontFragment newInstance(){
        Bundle bundle = new Bundle();
        CrazyFontFragment fontFragment = new CrazyFontFragment();
        fontFragment.setArguments(bundle);
        return fontFragment;
    }

    public CrazyFontFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_crazy_font, container, false);
        spinner = (Spinner)view.findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, fonts);
        spinner.setAdapter(adapter);
        return view;
    }

}
