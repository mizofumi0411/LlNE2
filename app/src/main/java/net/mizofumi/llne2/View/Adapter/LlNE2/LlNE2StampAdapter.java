package net.mizofumi.llne2.View.Adapter.LlNE2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import net.mizofumi.llne2.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mizofumi on 2017/06/11.
 */

public class LlNE2StampAdapter extends BaseAdapter {
    private ViewHolder viewHolder;
    private List<String> images = new ArrayList<>();
    private Context context;
    private LayoutInflater inflater;

    public LlNE2StampAdapter(Context context) {
        this.context = context;
        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public String getItem(int position) {
        return images.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void add(String image){
        images.add(image);
        notifyDataSetChanged();
    }

    public void add(int position,String image){
        images.add(position,image);
        notifyDataSetChanged();
    }

    public void remove(int position){
        images.remove(position);
        notifyDataSetChanged();
    }

    public void clear(){
        images.clear();
        notifyDataSetChanged();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null){
            convertView = inflater.inflate(R.layout.stamp_row,parent,false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        Picasso.with(context).load(images.get(position)).into(viewHolder.stampImageView);

        return convertView;
    }

    private class ViewHolder {
        ImageView stampImageView;
        public ViewHolder(View view) {
            stampImageView = (ImageView)view.findViewById(R.id.stampImageView);
        }
    }
}
