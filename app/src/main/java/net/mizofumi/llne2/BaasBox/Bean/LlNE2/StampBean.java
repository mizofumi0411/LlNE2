package net.mizofumi.llne2.BaasBox.Bean.LlNE2;

/**
 * Created by mizofumi on 2017/06/10.
 */

public class StampBean {

    private String id;
    private String imageId;
    private String userId;
    private String stickerId;
    private String imageUrl;
    private String sourceImageUrl;

    public StampBean(String id, String imageId, String userId, String stickerId, String imageUrl, String sourceImageUrl) {
        this.id = id;
        this.imageId = imageId;
        this.userId = userId;
        this.stickerId = stickerId;
        this.imageUrl = imageUrl;
        this.sourceImageUrl = sourceImageUrl;
    }

    public String getId() {
        return id;
    }

    public String getImageId() {
        return imageId;
    }

    public String getUserId() {
        return userId;
    }

    public String getStickerId() {
        return stickerId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getImage() {
        return sourceImageUrl;
    }

}
