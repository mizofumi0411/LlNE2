package net.mizofumi.llne2.View.Activity.Local.LlNE2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.GridView;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import net.mizofumi.llne2.BaasBox.API.BaasBoxAPI;
import net.mizofumi.llne2.BaasBox.Bean.LlNE2.StampBean;
import net.mizofumi.llne2.R;
import net.mizofumi.llne2.View.Adapter.Local.LlNE2.StampAdapter;

import java.util.List;


/*
    みぞサーバに保存したスタンプの詳細情報画面
 */
public class StickerDetailActivity extends AppCompatActivity {

    ImageView topImage;
    String stickerId;
    GridView gridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sticker_detail);
        stickerId = getIntent().getStringExtra("stickerId");
        gridView = ((GridView)findViewById(R.id.stamps));
        topImage = (ImageView)findViewById(R.id.topImage);
    }

    @Override
    protected void onResume() {
        super.onResume();

        final StampAdapter adapter = new StampAdapter(this);
        gridView.setAdapter(adapter);

        new BaasBoxAPI().getStamps(stickerId, new BaasBoxAPI.StampListReciveListener() {
            @Override
            public void onListen(List<StampBean> list) {
                if (list == null || list.size() == 0){
                    return;
                }
                Picasso.with(StickerDetailActivity.this).load(list.get(0).getImageUrl()).into(topImage);
                for (StampBean stamp : list) {
                    adapter.add(stamp);
                }
            }

            @Override
            public void onError() {

            }
        });
    }
}
