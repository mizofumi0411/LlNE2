package net.mizofumi.llne2;

import android.app.Application;

import com.baasbox.android.BaasBox;

/**
 * Created by mizofumi on 2017/06/10.
 */

public class MyApp extends Application {

    private BaasBox client;

    @Override
    public void onCreate() {
        super.onCreate();
        BaasBox.Builder builder = new BaasBox.Builder(this);
        client = builder.setApiDomain("geso.aa1.netvolante.jp").setApiBasepath("llne2/").setPort(80)
                .setAppCode("1234567890")
                .init();

    }
}
