package net.mizofumi.llne2.View.Adapter.LlNE2;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.mizofumi.llne2.LlNE2.API.LlNE2API;
import net.mizofumi.llne2.LlNE2.Sticker;
import net.mizofumi.llne2.R;
import net.mizofumi.llne2.View.Activity.LINE2.LlNE2StoreDetailActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mizofumi on 2017/06/11.
 */

/*
    LINEストアのスタンプ検索一覧のAdapter
 */
public class LlNE2StickersAdapter extends BaseAdapter {
    private ViewHolder viewHolder;
    private List<Sticker> list = new ArrayList<>();
    private Context context;
    private LayoutInflater inflater;

    public LlNE2StickersAdapter(Context context) {
        this.context = context;
        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void add(Sticker sticker){
        list.add(sticker);
        notifyDataSetChanged();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null){
            convertView = inflater.inflate(R.layout.llne2stickers_row,parent,false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        viewHolder.createType.setText(list.get(position).getCreateType().getJapaneseLabel());
        viewHolder.name.setText(list.get(position).getTitle());
        Picasso.with(context).load(list.get(position).getPreview()).into(viewHolder.imageView);
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LlNE2API.tmpSticker = list.get(position);
                context.startActivity(new Intent(context, LlNE2StoreDetailActivity.class));
            }
        });

        return convertView;
    }

    private class ViewHolder{
        TextView createType;
        TextView name;
        ImageView imageView;
        public ViewHolder(View view) {
            createType = (TextView)view.findViewById(R.id.createType);
            name = (TextView)view.findViewById(R.id.name);
            imageView = (ImageView)view.findViewById(R.id.imageView);
        }
    }
}
