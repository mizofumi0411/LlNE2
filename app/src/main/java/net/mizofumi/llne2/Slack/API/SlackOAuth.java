package net.mizofumi.llne2.Slack.API;

import android.os.Handler;
import android.os.Looper;

import net.mizofumi.llne2.Slack.SlackUserBean;
import net.mizofumi.llne2.Slack.SlackUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by mizofumi on 2017/06/10.
 */

public class SlackOAuth extends Thread {

    String requestToken;
    SlackOAuthListener listener;

    public SlackOAuth(String requestToken,SlackOAuthListener listener){
        this.requestToken = requestToken;
        this.listener = listener;
    }

    @Override
    public void run() {
        super.run();
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url("https://slack.com/api/oauth.access?client_id="+ SlackUtils.CLIENT_ID+"&client_secret="+ SlackUtils.CLIENT_SECRET+"&code="+requestToken)
                .build();

        try {
            Response response;
            response = client.newCall(request).execute();
            JSONObject rootObject = new JSONObject(response.body().string());
            final SlackUserBean bean = new SlackUserBean(rootObject.getString("access_token"),rootObject.getString("scope"),rootObject.getString("user_id"),rootObject.getString("team_name"),rootObject.getString("team_id"));
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    listener.success(bean);
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    listener.failed();
                }
            });
        } catch (JSONException e) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    listener.failed();
                }
            });
        }
    }

    public interface SlackOAuthListener {
        void success(SlackUserBean bean);
        void failed();
    }
}
