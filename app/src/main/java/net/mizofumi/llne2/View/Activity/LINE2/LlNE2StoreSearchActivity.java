package net.mizofumi.llne2.View.Activity.LINE2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.GridView;

import net.mizofumi.llne2.LlNE2.API.LlNE2API;
import net.mizofumi.llne2.LlNE2.Sticker;
import net.mizofumi.llne2.R;
import net.mizofumi.llne2.View.Adapter.LlNE2.LlNE2StickersAdapter;

import java.util.List;

public class LlNE2StoreSearchActivity extends AppCompatActivity {

    GridView gridView;
    LlNE2API.CREATE_TYPE createType;
    String word;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_llne2_store_search);
        gridView = (GridView)findViewById(R.id.gridView);

        switch (getIntent().getIntExtra("createType",0)){
            case 1:
                createType = LlNE2API.CREATE_TYPE.OFFICIAL;
                break;
            case 2:
                createType = LlNE2API.CREATE_TYPE.CREATEOR;
                break;
        }
        word = getIntent().getStringExtra("word");

    }

    @Override
    protected void onResume() {
        super.onResume();
        final LlNE2StickersAdapter adapter = new LlNE2StickersAdapter(this);
        gridView.setAdapter(adapter);
        new LlNE2API().getSearchStickers(word, createType, new LlNE2API.SearchStickersListener() {
            @Override
            public void success(List<Sticker> stickerBeanList) {
                for (Sticker sticker : stickerBeanList) {
                    adapter.add(sticker);
                }
            }

            @Override
            public void error() {

            }
        });

    }
}
