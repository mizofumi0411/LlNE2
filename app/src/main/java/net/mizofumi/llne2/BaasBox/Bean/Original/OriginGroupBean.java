package net.mizofumi.llne2.BaasBox.Bean.Original;

/**
 * Created by mizofumi on 2017/06/16.
 */

public class OriginGroupBean {

    private String id;
    private String creationDate;
    private String groupName;
    private String author;
    private String topImageId;
    private String topImageUri;
    private boolean isPrivate;

    public OriginGroupBean(String id, String creationDate, String groupName, String author, String topImageId, String topImageUri, boolean isPrivate) {
        this.id = id;
        this.creationDate = creationDate;
        this.groupName = groupName;
        this.author = author;
        this.topImageId = topImageId;
        this.topImageUri = topImageUri;
        this.isPrivate = isPrivate;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getTopImageId() {
        return topImageId;
    }

    public void setTopImageId(String topImageId) {
        this.topImageId = topImageId;
    }

    public String getTopImageUri() {
        return topImageUri;
    }

    public void setTopImageUri(String topImageUri) {
        this.topImageUri = topImageUri;
    }
}
