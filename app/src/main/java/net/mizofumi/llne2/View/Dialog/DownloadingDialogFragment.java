package net.mizofumi.llne2.View.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import net.mizofumi.llne2.LlNE2.API.LlNE2API;
import net.mizofumi.llne2.R;

/**
 * Created by mizofumi on 2017/06/11.
 */

public class DownloadingDialogFragment extends DialogFragment {

    ImageView previewImage;
    TextView progressText;
    ProgressBar progressBar;
    public static DownloadingDialogFragment newInstance(){
        Bundle bundle = new Bundle();
        DownloadingDialogFragment downloadingDialogFragment = new DownloadingDialogFragment();
        downloadingDialogFragment.setArguments(bundle);
        return downloadingDialogFragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String url = getArguments().getString("url");

        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_downloading,null);
        previewImage = (ImageView)view.findViewById(R.id.previewImage);
        progressText = (TextView)view.findViewById(R.id.progressText);
        progressBar = (ProgressBar)view.findViewById(R.id.progressBar);
        return new AlertDialog.Builder(getActivity())
                .setView(view)
                .create();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (LlNE2API.tmpSticker == null){
            Toast.makeText(getContext(), "StickerDataError", Toast.LENGTH_SHORT).show();
            dismiss();
        }

        new LlNE2API().downloadSticker(getContext(), LlNE2API.tmpSticker, new LlNE2API.DownloadStickerListener() {
            @Override
            public void onPreExecute() {
                Log.d("Dialog", "onPreExecute: ");
            }

            @Override
            public void onProgress(int now, int max, Bitmap image) {
                progressText.setText(now+"/"+max);
                previewImage.setImageBitmap(image);
                progressBar.setMax(max);
                progressBar.setProgress(now);
                Log.d("Dialog", "onProgress: "+now+"/"+max);
            }

            @Override
            public void finish() {
                Log.d("Dialog", "finish: ");
                Toast.makeText(getContext(), "保存しました", Toast.LENGTH_SHORT).show();
                dismiss();
            }

            @Override
            public void error() {
                Toast.makeText(getContext(), "画像の保存に失敗しました", Toast.LENGTH_SHORT).show();
                dismiss();
            }
        });
    }
}
