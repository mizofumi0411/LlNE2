package net.mizofumi.llne2.View.Activity.Local;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;

import net.mizofumi.llne2.R;
import net.mizofumi.llne2.View.Activity.LINE2.LlNE2StoreActivity;
import net.mizofumi.llne2.View.Activity.Local.Original.OriginalUploadActivity;
import net.mizofumi.llne2.View.Dialog.NewGroupDialog;
import net.mizofumi.llne2.View.Fragment.CrazyFontFragment;
import net.mizofumi.llne2.View.Fragment.OriginalFragment;
import net.mizofumi.llne2.View.Fragment.StickersFragment;

public class MainActivity extends AppCompatActivity implements AbsListView.OnScrollListener,NewGroupDialog.DialogCloseListener {

    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private FloatingActionButton fab;
    private ConstraintLayout content;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_original:
                    fragmentManager = getSupportFragmentManager();
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content, OriginalFragment.newInstance());
                    fragmentTransaction.commit();
                    fab.setImageResource(R.drawable.ic_add_white_24dp);
                    fab.show();
                    return true;
                case R.id.navigation_stickers:
                    fragmentManager = getSupportFragmentManager();
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content, StickersFragment.newInstance());
                    fragmentTransaction.commit();
                    fab.setImageResource(R.drawable.ic_local_grocery_store_white_24dp);
                    fab.show();
                    return true;
                case R.id.navigation_crazyfont:
                    fragmentManager = getSupportFragmentManager();
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content, CrazyFontFragment.newInstance());
                    fragmentTransaction.commit();
                    fab.hide();
                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setSelectedItemId(R.id.navigation_stickers);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        fab = (FloatingActionButton)findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (navigation.getSelectedItemId() == R.id.navigation_stickers){
                    startActivity(new Intent(MainActivity.this,LlNE2StoreActivity.class));
                }
                if (navigation.getSelectedItemId() == R.id.navigation_original){
//                    startActivity(new Intent(MainActivity.this,OriginalUploadActivity.class));
                    NewGroupDialog dialog = NewGroupDialog.newInstance();
                    dialog.show(getSupportFragmentManager(),"newGroup");
                }
            }
        });
        content = (ConstraintLayout)findViewById(R.id.content);


        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content, StickersFragment.newInstance());
        fragmentTransaction.commit();
        fab.show();

    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        if (SCROLL_STATE_IDLE == scrollState){
            fab.show();
        }else {
            fab.hide();
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

    }

    @Override
    public void handleDialogClose(DialogInterface dialogInterface) {
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content, OriginalFragment.newInstance());
        fragmentTransaction.commit();
    }
}
