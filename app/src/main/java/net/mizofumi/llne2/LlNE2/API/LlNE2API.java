package net.mizofumi.llne2.LlNE2.API;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.baasbox.android.BaasDocument;
import com.baasbox.android.BaasFile;
import com.baasbox.android.BaasHandler;
import com.baasbox.android.BaasResult;
import com.baasbox.android.json.JsonObject;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import net.mizofumi.llne2.LlNE2.Sticker;
import net.mizofumi.llne2.SharedPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by mizofumi on 2017/06/10.
 */

public class LlNE2API {

    public static Sticker tmpSticker;

    public void getTopStickers(final CREATE_TYPE createType, final TopStickersListener listener){

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    final List<Sticker> stickers = new ArrayList<Sticker>();
                    Document document = null;
                    document = Jsoup.connect("https://store.line.me/stickershop/showcase/"+createType.getTopLabel()+"/ja")
                            .userAgent("Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36")
                            .get();
                    Elements mdCMN02Li = document.getElementsByClass("mdCMN02Li");
                    for (Element element : mdCMN02Li){
                        Sticker sticker = new Sticker();
                        sticker.setSource("https://store.line.me/"+element.getElementsByTag("a").attr("href"));
                        sticker.setTitle(element.getElementsByClass("mdCMN05Ttl").text());
                        sticker.setCreateType(createType);
                        sticker.setPreview(element.getElementsByTag("img").attr("src"));
                        stickers.add(sticker);
                    }
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            listener.success(stickers);
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            listener.error();
                        }
                    });
                }
            }
        }).start();

    }
    public interface TopStickersListener extends StickersListener{
    }

    public void getSearchStickers(final String word, final CREATE_TYPE createType, final SearchStickersListener listener){

        new Thread(new Runnable() {
            @Override
            public void run() {

                int max = getTotalCount(word,createType);
                try {
                    final List<Sticker> stickers = new ArrayList<Sticker>();
                    String query = URLEncoder.encode(word,"UTF-8");
                    OkHttpClient client = new OkHttpClient();
                    Request request = new Request.Builder()
                            .addHeader("cookie","store_lang=ja;display_lang=ja")
                            .url("https://store.line.me/api/search/sticker?query="+query+"&offset=0&limit="+max+"&type="+createType.getSearchLabel()+"&includeFacets=false")
                            .build();
                    try {
                        Response response = client.newCall(request).execute();
                        JSONObject rootObject = new JSONObject(response.body().string());
                        JSONArray items = rootObject.getJSONArray("items");
                        System.out.println(items.length());
                        for (int i = 0; i < items.length(); i++) {
                            JSONObject item = items.getJSONObject(i);

                            Sticker sticker = new Sticker();
                            sticker.setSource("https://store.line.me/"+item.getString("productUrl"));
                            sticker.setTitle(item.getString("title"));
                            sticker.setCreateType(createType);
                            sticker.setPreview(item.getJSONObject("listIcon").getString("src"));
                            stickers.add(sticker);
                        }
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                listener.success(stickers);
                            }
                        });
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                listener.error();
                            }
                        });
                    }
                } catch (UnsupportedEncodingException e) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            listener.error();
                        }
                    });
                }


            }
        }).start();

    }
    public interface SearchStickersListener extends TopStickersListener {
    }

    private int getTotalCount(String word,CREATE_TYPE createType){
        try {
            String query = URLEncoder.encode(word,"UTF-8");
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .addHeader("cookie","store_lang=ja;display_lang=ja")
                    .url("https://store.line.me/api/search/sticker?query="+query+"&offset=0&limit=1&type="+createType.getSearchLabel()+"&includeFacets=false")
                    .build();
            try {
                Response response = client.newCall(request).execute();
//                        System.out.println(response.body().string());
                JSONObject rootObject = new JSONObject(response.body().string());
                return rootObject.getInt("totalCount");
            } catch (IOException | JSONException e) {
                e.printStackTrace();
                return 0;
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public void getStickerDetail(final Sticker sticker, final StickerDetailListener listener){
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    Document document = Jsoup.connect(sticker.getSource())
                            .userAgent("Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36")
                            .get();
                    sticker.setDetail(document.getElementsByClass("mdCMN08Desc").text());
                    Elements mdCMN09Image = document.getElementsByClass("mdCMN09Image");
                    int now = 0;
                    for (Element e : mdCMN09Image){
                        sticker.add(e.attr("style").split(" ")[4].replaceAll("background-image:url\\(", "").replaceAll("\\);", ""));
                        now++;
                    }
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            listener.success(sticker);
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            listener.error();
                        }
                    });
                }

            }
        }).start();
    }

    public interface StickerDetailListener{
        void success(Sticker sticker);
        void error();
    }


    /*
        スタンプ画像の取得とみぞサーバへ転送
     */
    public void downloadSticker(final Context context, final Sticker sticker, final DownloadStickerListener listener){
        listener.onPreExecute();
        new Thread(new Runnable() {
            @Override
            public void run() {
                BaasDocument document = new BaasDocument("Stickers");
                document.put("userId",SharedPref.loadUserId(context))
                        .put("name",sticker.getTitle())
                        .put("rating",0)
                        .put("sourceUrl",sticker.getSource())
                        .put("topImage",sticker.getImages().get(0))
                        .put("detail",sticker.getDetail());
                document.save(new BaasHandler<BaasDocument>() {
                    @Override
                    public void handle(final BaasResult<BaasDocument> stickerResult) {
                        if (stickerResult.isSuccess()){

                            for (int i = 0; i < sticker.getImages().size(); i++) {
                                final int nowIndex = i;
                                Picasso.with(context).load(sticker.getImages().get(i)).into(new Target() {
                                    @Override
                                    public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {

                                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                        bitmap.compress(Bitmap.CompressFormat.PNG,100,byteArrayOutputStream);
                                        final byte[] bytes = byteArrayOutputStream.toByteArray();

                                        JsonObject attachedData = new JsonObject()
                                                .put("userId", SharedPref.loadUserId(context))
                                                .put("stickerId",stickerResult.value().getId())
                                                .put("sourceImageUrl",sticker.getImages().get(nowIndex));
                                        BaasFile file = new BaasFile(attachedData);
                                        file.upload(bytes, new BaasHandler<BaasFile>() {
                                            @Override
                                            public void handle(BaasResult<BaasFile> imageResult) {
                                                if (imageResult.isSuccess()){
                                                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            listener.onProgress(nowIndex,sticker.getImages().size(),bitmap);
                                                        }
                                                    });
                                                }else {
                                                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            listener.error();
                                                        }
                                                    });
                                                }
                                            }
                                        });
                                    }

                                    @Override
                                    public void onBitmapFailed(Drawable errorDrawable) {
                                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                                            @Override
                                            public void run() {
                                                listener.error();
                                            }
                                        });
                                    }

                                    @Override
                                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                                    }
                                });
                            }
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    listener.finish();
                                }
                            });
                        }else {
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    listener.error();
                                }
                            });
                        }
                    }
                });
            }
        }).start();

    }

    public interface DownloadStickerListener{
        void onPreExecute();
        void onProgress(int now,int max,Bitmap image);
        void finish();
        void error();
    }

    public interface StickersListener {
        void success(List<Sticker> stickerBeanList);
        void error();
    }
    public enum CREATE_TYPE{
        OFFICIAL, CREATEOR;

        private String getTopLabel(){
            switch (this){
                case OFFICIAL:
                    return "new";
                case CREATEOR:
                    return "new_creators";
                default:
                    return "";
            }
        }

        private String getSearchLabel(){
            switch (this){
                case OFFICIAL:
                    return "OFFICIAL";
                case CREATEOR:
                    return "CREATORS";
                default:
                    return "";
            }
        }

        public String getJapaneseLabel(){
            switch (this){
                case OFFICIAL:
                    return "公式";
                case CREATEOR:
                    return "クリエイター";
                default:
                    return "";
            }
        }

        public int getId(){
            switch (this){
                case OFFICIAL:
                    return 1;
                case CREATEOR:
                    return 2;
                default:
                    return 0;
            }
        }

        public CREATE_TYPE getCreateType(int id){
            switch (id){
                case 1:
                    return OFFICIAL;
                case 2:
                    return CREATEOR;
                default:
                    return null;
            }
        }
    }
}
