package net.mizofumi.llne2;

import android.content.Context;

/**
 * Created by mizofumi on 2017/06/10.
 */

public class SharedPref {


    public static void saveAccessToken(Context context,String accessToken){
        context.getSharedPreferences("LLNE2",Context.MODE_PRIVATE).edit().putString("accessToken",accessToken).apply();
    }

    public static void saveUserId(Context context,String userId){
        context.getSharedPreferences("LLNE2",Context.MODE_PRIVATE).edit().putString("userId",userId).apply();
    }

    public static String loadAccessToken(Context context){
        return context.getSharedPreferences("LLNE2",Context.MODE_PRIVATE).getString("accessToken",null);
    }

    public static String loadUserId(Context context){
        return context.getSharedPreferences("LLNE2",Context.MODE_PRIVATE).getString("userId",null);
    }
}
