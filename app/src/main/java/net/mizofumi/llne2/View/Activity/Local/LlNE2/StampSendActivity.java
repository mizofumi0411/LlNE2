package net.mizofumi.llne2.View.Activity.Local.LlNE2;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import net.mizofumi.llne2.R;
import net.mizofumi.llne2.Slack.API.SlackChannel;
import net.mizofumi.llne2.Slack.API.SlackFilePost;
import net.mizofumi.llne2.Slack.API.SlackPost;
import net.mizofumi.llne2.Slack.SlackChannelBean;

import java.io.ByteArrayOutputStream;
import java.util.List;

public class StampSendActivity extends AppCompatActivity {

    Switch filePost;
    boolean isFilePost = true;
    ListView listView;
    FloatingActionButton fab;
    String imageUrl;
    String sourceUrl;
    Bitmap image;
    ImageView imageView;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stamp_send);

        imageUrl = getIntent().getStringExtra("imageUrl");
        sourceUrl = getIntent().getStringExtra("sourceUrl");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        filePost = (Switch)findViewById(R.id.filePost);
        filePost.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isFilePost = isChecked;
            }
        });
        button = (Button)findViewById(R.id.button);
        button.setVisibility(View.INVISIBLE);
        imageView = (ImageView)findViewById(R.id.imageView);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.hide();

        Log.d("S",sourceUrl);

        Picasso.with(this).load(imageUrl).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                StampSendActivity.this.image = bitmap;
                imageView.setImageBitmap(bitmap);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });

        listView = (ListView) findViewById(R.id.listView);
    }

    @Override
    protected void onResume() {
        super.onResume();

        new SlackChannel().getChannels(this, new SlackChannel.SlackChannelListListener() {
            @Override
            public void success(final List<SlackChannelBean> list) {
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(StampSendActivity.this, android.R.layout.simple_list_item_1);
                for (SlackChannelBean channel : list) {
                    adapter.add(channel.getName());
                }
                listView.setAdapter(adapter);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String name = list.get(position).getName();
                        String channelId = list.get(position).getId();
                        if (isFilePost){
                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            image.compress(Bitmap.CompressFormat.PNG,100,byteArrayOutputStream);
                            final byte[] bytes = byteArrayOutputStream.toByteArray();
                            new SlackFilePost().sendStamp(StampSendActivity.this,name,bytes,channelId,null);
                        }else {
                            new SlackPost().sendStamp(StampSendActivity.this,sourceUrl,channelId);
                        }
                        finish();
                    }
                });
//                fab.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        int index = listView.getSelectedItemPosition();
//                        String name = list.get(index).getName();
//                        String id = list.get(index).getId();
//                        new SlackPost().sendStamp(StampSendActivity.this,sourceUrl,id);
//                        finish();
//                    }
//                });
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ClipboardManager clipboardManager = (ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
                        clipboardManager.setPrimaryClip(ClipData.newPlainText("label", sourceUrl));
                        Toast.makeText(StampSendActivity.this, "コピーしました", Toast.LENGTH_SHORT).show();
                    }
                });
                button.setVisibility(View.VISIBLE);
//                fab.show();
            }

            @Override
            public void error() {

            }
        });
    }
}
