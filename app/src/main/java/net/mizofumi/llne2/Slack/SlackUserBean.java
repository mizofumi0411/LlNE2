package net.mizofumi.llne2.Slack;

/**
 * Created by mizofumi on 2017/06/10.
 */

public class SlackUserBean {
    private String accessToken;
    private String scope;
    private String userId;
    private String teamName;
    private String teamId;

    public SlackUserBean(String accessToken, String scope, String userId, String teamName, String teamId) {
        this.accessToken = accessToken;
        this.scope = scope;
        this.userId = userId;
        this.teamName = teamName;
        this.teamId = teamId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getScope() {
        return scope;
    }

    public String getUserId() {
        return userId;
    }

    public String getTeamName() {
        return teamName;
    }

    public String getTeamId() {
        return teamId;
    }


    @Override
    public String toString() {
        return "SlackUserBean{" +
                "accessToken='" + accessToken + '\'' +
                ", scope='" + scope + '\'' +
                ", userId='" + userId + '\'' +
                ", teamName='" + teamName + '\'' +
                ", teamId='" + teamId + '\'' +
                '}';
    }
}
