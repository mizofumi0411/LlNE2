package net.mizofumi.llne2.View.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.baasbox.android.BaasDocument;
import com.baasbox.android.BaasHandler;
import com.baasbox.android.BaasResult;

import net.mizofumi.llne2.BaasBox.API.BaasBoxAPI;
import net.mizofumi.llne2.R;
import net.mizofumi.llne2.SharedPref;
import net.mizofumi.llne2.View.Activity.Local.Original.OriginalUploadActivity;

/**
 * Created by mizofumi on 2017/06/15.
 */

public class NewGroupDialog extends DialogFragment {

    boolean isPrivate = false;
    EditText groupName;
    Switch privateSwitch;
    Button send;

    public static NewGroupDialog newInstance(){
        Bundle bundle = new Bundle();
        NewGroupDialog dialog = new NewGroupDialog();
        dialog.setArguments(bundle);
        return dialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_origin_group,null);
        groupName = (EditText)view.findViewById(R.id.groupName);
        privateSwitch = (Switch)view.findViewById(R.id.privateSwitch);
        privateSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isPrivate = isChecked;
            }
        });
        send = (Button)view.findViewById(R.id.send);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (groupName.getText().toString().isEmpty()){
                    Toast.makeText(getActivity(), "グループ名を入力してください", Toast.LENGTH_SHORT).show();
                    return;
                }

                new BaasBoxAPI().sendOriginGroup(groupName.getText().toString(), isPrivate, SharedPref.loadUserId(getActivity()), new BaasHandler<BaasDocument>() {
                    @Override
                    public void handle(BaasResult<BaasDocument> baasResult) {
                        if (baasResult.isSuccess()){
                            Toast.makeText(getActivity(), "作成しました", Toast.LENGTH_SHORT).show();
                            dismiss();
                        }else {
                            Toast.makeText(getActivity(), "作成に失敗しました", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });

        return new AlertDialog.Builder(getContext())
                .setView(view).create();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (getActivity() instanceof OriginalUploadActivity){
//            ((DialogCloseListener)getActivity()).handleDialogClose(dialog);
        }
        try {
            ((DialogCloseListener)getActivity()).handleDialogClose(dialog);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public interface DialogCloseListener{
        void handleDialogClose(DialogInterface dialogInterface);
    }
}
