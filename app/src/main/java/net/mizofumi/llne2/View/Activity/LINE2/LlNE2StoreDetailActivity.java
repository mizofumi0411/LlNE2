package net.mizofumi.llne2.View.Activity.LINE2;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import net.mizofumi.llne2.LlNE2.API.LlNE2API;
import net.mizofumi.llne2.LlNE2.Sticker;
import net.mizofumi.llne2.R;
import net.mizofumi.llne2.View.Adapter.LlNE2.LlNE2StampAdapter;
import net.mizofumi.llne2.View.Dialog.DownloadingDialogFragment;


/*
LINEストアのスタンプ詳細画面
 */
public class LlNE2StoreDetailActivity extends AppCompatActivity {

    ImageView topImage;
    TextView name;
    TextView detail;
    GridView gridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_llne2_store_detail);

        if (LlNE2API.tmpSticker == null){
            Toast.makeText(this, "ステッカー情報の取得に失敗", Toast.LENGTH_SHORT).show();
            finish();
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DownloadingDialogFragment.newInstance().show(getSupportFragmentManager(),"downloading");
            }
        });
        name = ((TextView)findViewById(R.id.name));
        gridView = ((GridView)findViewById(R.id.stamps));
        topImage = (ImageView)findViewById(R.id.topImage);
        detail = (TextView)findViewById(R.id.detail);
    }

    @Override
    protected void onResume() {
        super.onResume();

        final LlNE2StampAdapter adapter = new LlNE2StampAdapter(this);
        gridView.setAdapter(adapter);

        new LlNE2API().getStickerDetail(LlNE2API.tmpSticker, new LlNE2API.StickerDetailListener() {
            @Override
            public void success(Sticker sticker) {
                name.setText(sticker.getTitle());
                detail.setText(sticker.getDetail());
                Picasso.with(LlNE2StoreDetailActivity.this).load(sticker.getPreview()).into(topImage);
                for (String image : sticker.getImages()) {
                    adapter.add(image);
                }
            }

            @Override
            public void error() {

            }
        });

    }
}
