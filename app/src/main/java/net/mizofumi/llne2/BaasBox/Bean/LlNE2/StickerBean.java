package net.mizofumi.llne2.BaasBox.Bean.LlNE2;

/**
 * Created by mizofumi on 2017/06/10.
 */

public class StickerBean {

    private String id;
    private String topImage;
    private String userId;
    private String name;
    private String detail;
    private String sourceUrl;
    private float rating;

    public StickerBean(String id, String userId, String name, String detail, String sourceUrl, String topImage,float rating) {
        this.id = id;
        this.userId = userId;
        this.name = name;
        this.detail = detail;
        this.sourceUrl = sourceUrl;
        this.topImage = topImage;
        this.rating = rating;
    }

    public StickerBean() {
    }

    public String getId() {
        return id;
    }

    public String getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public String getDetail() {
        return detail;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public float getRating() {
        return rating;
    }

    public StickerBean setId(String id) {
        this.id = id;
        return this;
    }

    public String getTopImage() {
        return topImage;
    }

    public StickerBean setTopImage(String topImage) {
        this.topImage = topImage;
        return this;
    }

    public StickerBean setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public StickerBean setName(String name) {
        this.name = name;
        return this;
    }

    public StickerBean setDetail(String detail) {
        this.detail = detail;
        return this;
    }

    public StickerBean setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
        return this;
    }

    public StickerBean setRating(float rating) {
        this.rating = rating;
        return this;
    }
}
