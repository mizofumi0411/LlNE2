package net.mizofumi.llne2.View.Activity.Local;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.baasbox.android.BaasHandler;
import com.baasbox.android.BaasResult;
import com.baasbox.android.BaasUser;

import net.mizofumi.llne2.R;
import net.mizofumi.llne2.SharedPref;
import net.mizofumi.llne2.Slack.API.SlackOAuth;
import net.mizofumi.llne2.Slack.SlackUserBean;
import net.mizofumi.llne2.Slack.SlackUtils;

public class OAuthActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oauth);


        (findViewById(R.id.slackLogin)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://slack.com/oauth/authorize?client_id="+ SlackUtils.CLIENT_ID+"&scope=chat:write:user,channels:read,files:write:user,groups:read"));
                startActivity(intent);
            }
        });
    }


    @Override
    protected void onNewIntent(Intent intent) {
        if (intent == null || intent.getData() == null){
            Toast.makeText(this, "認証情報を取得できませんでした", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        String code = intent.getData().getQueryParameter("code");
        new SlackOAuth(code, new SlackOAuth.SlackOAuthListener() {
            @Override
            public void success(final SlackUserBean bean) {

                final BaasUser baasUser = BaasUser.withUserName(bean.getUserId()).setPassword("llne2");
                baasUser.login(new BaasHandler<BaasUser>() {
                    @Override
                    public void handle(BaasResult<BaasUser> baasResult) {
                        if (baasResult.isSuccess()){
                            oauthSuccess(bean);
                        }else {
                            baasUser.signup(new BaasHandler<BaasUser>() {
                                @Override
                                public void handle(BaasResult<BaasUser> baasResult) {
                                    if (baasResult.isSuccess()){
                                        oauthSuccess(bean);
                                    }else if (baasResult.isFailed()){
                                        oauthFailed("みぞサーバとの通信に失敗しました");
                                    }
                                }
                            });
                        }
                    }
                });
            }
            @Override
            public void failed() {
                Log.d("SlackOAuth", "failed: ");
                oauthFailed("Slack認証失敗...");
            }
        }).start();
    }

    private void oauthSuccess(SlackUserBean bean){
        SharedPref.saveUserId(OAuthActivity.this,bean.getUserId());
        SharedPref.saveAccessToken(OAuthActivity.this,bean.getAccessToken());
        Toast.makeText(OAuthActivity.this,"認証完了!", Toast.LENGTH_SHORT).show();
        finish();
        startActivity(new Intent(OAuthActivity.this,StartupActivity.class));
    }

    private void oauthFailed(String message){
        Toast.makeText(OAuthActivity.this, message, Toast.LENGTH_SHORT).show();
        startActivity(new Intent(OAuthActivity.this,OAuthActivity.class));
        finish();
    }
}
