package net.mizofumi.llne2.View.Adapter.Local.Original;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.mizofumi.llne2.BaasBox.Bean.Original.OriginGroupBean;
import net.mizofumi.llne2.R;
import net.mizofumi.llne2.View.Activity.Local.Original.OriginalDetailActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mizofumi on 2017/06/17.
 */

public class OriginalGroupsAdapter extends BaseAdapter {
    private OriginalGroupsAdapter.ViewHolder viewHolder;
    private Context context;
    private List<OriginGroupBean> groupBeanList = new ArrayList<>();
    private LayoutInflater inflater;

    public OriginalGroupsAdapter(Context context) {
        this.context = context;
        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return groupBeanList.size();
    }

    @Override
    public Object getItem(int position) {
        return groupBeanList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void add(OriginGroupBean groupBean){
        groupBeanList.add(groupBean);
        notifyDataSetChanged();
    }

    public void clear(){
        groupBeanList.clear();
        notifyDataSetChanged();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null){
            convertView = inflater.inflate(R.layout.origstickers_row,parent,false);
            viewHolder = new OriginalGroupsAdapter.ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (OriginalGroupsAdapter.ViewHolder)convertView.getTag();
        }

        //Log.d("StickersAdapter", "getView: "+groupBeanList.get(position).getTopImage());
        viewHolder.name.setText(groupBeanList.get(position).getGroupName());
        if (groupBeanList.get(position).isPrivate()){
            viewHolder.privateImage.setImageResource(R.drawable.ic_lock_white_24dp);
        }else {
            viewHolder.privateImage.setImageBitmap(null);
        }

        viewHolder.imageView.setTag(groupBeanList.get(position).getGroupName());

        Picasso.with(context).load(groupBeanList.get(position).getTopImageUri()).into(viewHolder.imageView);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, OriginalDetailActivity.class);
                intent.putExtra("groupId",groupBeanList.get(position).getId());
                intent.putExtra("groupName",groupBeanList.get(position).getGroupName());
                intent.putExtra("topImageUri",groupBeanList.get(position).getTopImageUri());
                context.startActivity(intent);
            }
        });
        return convertView;
    }

    class ViewHolder{
        TextView name;
        ImageView privateImage;
        ImageView imageView;
        public ViewHolder(View view) {
            name = (TextView)view.findViewById(R.id.name);
            privateImage = (ImageView) view.findViewById(R.id.privateImage);
            imageView = (ImageView)view.findViewById(R.id.imageView);
        }
    }
}
