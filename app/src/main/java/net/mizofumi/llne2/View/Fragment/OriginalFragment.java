package net.mizofumi.llne2.View.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import net.mizofumi.llne2.BaasBox.API.BaasBoxAPI;
import net.mizofumi.llne2.BaasBox.Bean.Original.OriginGroupBean;
import net.mizofumi.llne2.R;
import net.mizofumi.llne2.SharedPref;
import net.mizofumi.llne2.View.Adapter.Local.Original.OriginalGroupsAdapter;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class OriginalFragment extends Fragment {

    GridView gridView;
    private OriginalGroupsAdapter adapter;

    public static OriginalFragment newInstance(){
        Bundle bundle = new Bundle();
        OriginalFragment fragment = new OriginalFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    public OriginalFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_original, container, false);
        gridView = (GridView)view.findViewById(R.id.stickers);
        return view;

    }

    @Override
    public void onResume() {
        super.onResume();
        adapter = new OriginalGroupsAdapter(getActivity());
        adapter.clear();
        gridView.setAdapter(adapter);
        new BaasBoxAPI().getOriginGroups(SharedPref.loadUserId(getActivity()), true, new BaasBoxAPI.OriginGroupListReciveListener() {
            @Override
            public void onListen(List<OriginGroupBean> list) {
                for (OriginGroupBean bean : list) {
                    adapter.add(bean);
                }
                new BaasBoxAPI().getOriginGroups(SharedPref.loadUserId(getActivity()), false, new BaasBoxAPI.OriginGroupListReciveListener() {
                    @Override
                    public void onListen(List<OriginGroupBean> list) {
                        for (OriginGroupBean bean : list) {
                            adapter.add(bean);
                        }
                    }

                    @Override
                    public void onError() {

                    }
                });
            }

            @Override
            public void onError() {

            }
        });

    }
}
