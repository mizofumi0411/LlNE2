package net.mizofumi.llne2.View.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import net.mizofumi.llne2.BaasBox.API.BaasBoxAPI;
import net.mizofumi.llne2.BaasBox.Bean.LlNE2.StickerBean;
import net.mizofumi.llne2.R;
import net.mizofumi.llne2.SharedPref;
import net.mizofumi.llne2.View.Activity.Local.MainActivity;
import net.mizofumi.llne2.View.Adapter.Local.LlNE2.StickersAdapter;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class StickersFragment extends Fragment {


    GridView gridView;

    public static StickersFragment newInstance(){
        Bundle bundle = new Bundle();
        StickersFragment fragment = new StickersFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    public StickersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_stickers, container, false);

        gridView = ((GridView)view.findViewById(R.id.stickers));
        gridView.setOnScrollListener((MainActivity)getActivity());
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        final StickersAdapter adapter = new StickersAdapter(getActivity());
        gridView.setAdapter(adapter);
        new BaasBoxAPI().getStickers(SharedPref.loadUserId(getActivity()), new BaasBoxAPI.StickerListReciveListener() {
            @Override
            public void onListen(List<StickerBean> list) {
                for (StickerBean stickerBean : list) {
                    adapter.add(stickerBean);
                }
            }

            @Override
            public void onError() {

            }
        });
    }
}
