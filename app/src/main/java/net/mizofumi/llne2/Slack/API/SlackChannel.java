package net.mizofumi.llne2.Slack.API;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import net.mizofumi.llne2.SharedPref;
import net.mizofumi.llne2.Slack.SlackChannelBean;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by mizofumi on 2017/06/11.
 */

public class SlackChannel {


    public void getChannels(final Context context, final SlackChannelListListener listener){

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    final List<SlackChannelBean> channelList = new ArrayList<SlackChannelBean>();
                    OkHttpClient client = new OkHttpClient();
                    Request request = new Request.Builder()
                            .url("https://slack.com/api/channels.list?token="+ SharedPref.loadAccessToken(context)+"")
                            .build();
                    Response response = client.newCall(request).execute();
                    JSONObject rootObject = new JSONObject(response.body().string());
                    JSONArray channels = rootObject.getJSONArray("channels");
                    for (int i = 0; i < channels.length(); i++) {
                        JSONObject channel = channels.getJSONObject(i);
                        Log.d("getChannels",channel.getString("name"));
                        Log.d("getChannels",channel.getString("id"));
                        channelList.add(new SlackChannelBean(channel.getString("id"),channel.getString("name")));
                    }


                    request = new Request.Builder()
                            .url("https://slack.com/api/groups.list?token="+ SharedPref.loadAccessToken(context)+"")
                            .build();
                    response = client.newCall(request).execute();
                    rootObject = new JSONObject(response.body().string());
                    JSONArray groups = rootObject.getJSONArray("groups");
                    for (int i = 0; i < groups.length(); i++) {
                        JSONObject group = groups.getJSONObject(i);
                        Log.d("getChannels",group.getString("name"));
                        Log.d("getChannels",group.getString("id"));
                        channelList.add(new SlackChannelBean(group.getString("id"),group.getString("name")));
                    }
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            listener.success(channelList);
                        }
                    });
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            listener.error();
                        }
                    });
                }
            }
        }).start();

    }

    public interface SlackChannelListListener {
        void success(List<SlackChannelBean> list);
        void error();
    }
}
