package net.mizofumi.llne2.Slack.API;

import android.content.Context;

import net.mizofumi.llne2.SharedPref;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by mizofumi on 2017/06/11.
 */

public class SlackPost {

    public void sendStamp(final Context context, final String stampUrl, final String channel){
        new Thread(new Runnable() {
            @Override
            public void run() {
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url("https://slack.com/api/chat.postMessage?token="+ SharedPref.loadAccessToken(context)+"&channel="+channel+"&as_user=true&text="+stampUrl)
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
