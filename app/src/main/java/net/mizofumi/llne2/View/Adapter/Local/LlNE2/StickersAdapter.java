package net.mizofumi.llne2.View.Adapter.Local.LlNE2;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.mizofumi.llne2.BaasBox.Bean.LlNE2.StickerBean;
import net.mizofumi.llne2.R;
import net.mizofumi.llne2.View.Activity.Local.LlNE2.StickerDetailActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mizofumi on 2017/06/10.
 */

public class StickersAdapter extends BaseAdapter {
    private ViewHolder viewHolder;
    private Context context;
    private LayoutInflater inflater;
    private List<StickerBean> list = new ArrayList<>();

    public StickersAdapter(Context context) {
        this.context = context;
        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public StickerBean getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void add(StickerBean stickersBean){
        list.add(stickersBean);
        notifyDataSetChanged();
    }

    public void add(int position,StickerBean stickersBean){
        list.add(position,stickersBean);
        notifyDataSetChanged();
    }

    public void remove(int position){
        list.remove(position);
        notifyDataSetChanged();
    }

    public void clear(){
        list.clear();
        notifyDataSetChanged();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null){
            convertView = inflater.inflate(R.layout.stickers_row,parent,false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        Log.d("StickersAdapter", "getView: "+list.get(position).getTopImage());
        viewHolder.name.setText(list.get(position).getName());
        viewHolder.ratingBar.setRating(list.get(position).getRating());

        Picasso.with(context).load(list.get(position).getTopImage()).into(viewHolder.imageView);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, StickerDetailActivity.class);
                intent.putExtra("stickerId",list.get(position).getId());
                context.startActivity(intent);
            }
        });

        return convertView;
    }

    class ViewHolder{
        TextView name;
        RatingBar ratingBar;
        ImageView imageView;
        public ViewHolder(View view) {
            name = (TextView)view.findViewById(R.id.name);
            ratingBar = (RatingBar)view.findViewById(R.id.ratingBar);
            imageView = (ImageView)view.findViewById(R.id.imageView);
        }
    }
}
