package net.mizofumi.llne2.Slack.API;

import android.content.Context;
import android.util.Log;

import net.mizofumi.llne2.SharedPref;
import net.mizofumi.llne2.View.Activity.Local.LlNE2.StampSendActivity;

import java.io.File;
import java.io.IOException;

import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by mizofumi on 2017/06/17.
 */

public class SlackFilePost {

    public void sendStamp(final Context context, final String name, final byte[] bytes, final String channel, final SendStampResultHandler handler){


        new Thread(new Runnable() {
            @Override
            public void run() {
                MultipartBody multipartBody = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addPart(
                                Headers.of("Content-Disposition", "form-data; name=\"file\"; filename=\"qp8ND78.png\"" ),
                                RequestBody.create(MediaType.parse("image/png"), bytes))
                        .build();

                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url("https://slack.com/api/files.upload?token="+ SharedPref.loadAccessToken(context)+"&channels="+channel+"&filetype=png&filename=llne2.png")
                        .post(multipartBody)
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                    if (response.isSuccessful()){
                        if (handler != null)
                            handler.success();
                    }else {
                        if (handler != null)
                            handler.failed();
                    }
                    Log.d("SlackFilePost", "run: "+response.message());
                    Log.d("SlackFilePost", "run: "+response.body().string());
                } catch (IOException e) {
                    if (handler != null)
                        handler.failed();
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public interface SendStampResultHandler{
        void success();
        void failed();
    }

}
