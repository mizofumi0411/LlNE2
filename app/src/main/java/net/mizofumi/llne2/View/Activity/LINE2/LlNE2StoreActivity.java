package net.mizofumi.llne2.View.Activity.LINE2;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.GridView;

import net.mizofumi.llne2.LlNE2.API.LlNE2API;
import net.mizofumi.llne2.LlNE2.Sticker;
import net.mizofumi.llne2.R;
import net.mizofumi.llne2.View.Adapter.LlNE2.LlNE2StickersAdapter;

import java.util.List;

/*
    LINEストアのトップ画面
 */

public class LlNE2StoreActivity extends AppCompatActivity {

    GridView gridView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final EditText editText = new EditText(LlNE2StoreActivity.this);
                AlertDialog.Builder dialog = new AlertDialog.Builder(LlNE2StoreActivity.this);
                dialog.setTitle("検索");
                dialog.setView(editText);
                dialog.setPositiveButton(LlNE2API.CREATE_TYPE.OFFICIAL.getJapaneseLabel(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(LlNE2StoreActivity.this,LlNE2StoreSearchActivity.class);
                        intent.putExtra("word",editText.getText().toString());
                        intent.putExtra("createType", LlNE2API.CREATE_TYPE.OFFICIAL.getId());
                        startActivity(intent);
                    }
                });
                dialog.setNegativeButton(LlNE2API.CREATE_TYPE.CREATEOR.getJapaneseLabel(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(LlNE2StoreActivity.this,LlNE2StoreSearchActivity.class);
                        intent.putExtra("word",editText.getText().toString());
                        intent.putExtra("createType", LlNE2API.CREATE_TYPE.CREATEOR.getId());
                        startActivity(intent);
                    }
                });
                dialog.show();
            }
        });

        gridView = (GridView)findViewById(R.id.stickers);
    }

    @Override
    protected void onResume() {
        super.onResume();
        final LlNE2StickersAdapter adapter = new LlNE2StickersAdapter(this);
        gridView.setAdapter(adapter);
        new LlNE2API().getTopStickers(LlNE2API.CREATE_TYPE.OFFICIAL, new LlNE2API.TopStickersListener() {
            @Override
            public void success(List<Sticker> stickerBeanList) {
                for (Sticker s : stickerBeanList) {
                    adapter.add(s);
                }
            }

            @Override
            public void error() {

            }
        });
        new LlNE2API().getTopStickers(LlNE2API.CREATE_TYPE.CREATEOR, new LlNE2API.TopStickersListener() {
            @Override
            public void success(List<Sticker> stickerBeanList) {
                for (Sticker s : stickerBeanList) {
                    adapter.add(s);
                }
            }

            @Override
            public void error() {

            }
        });

    }
}
