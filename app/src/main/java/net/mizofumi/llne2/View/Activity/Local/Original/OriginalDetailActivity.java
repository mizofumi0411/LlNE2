package net.mizofumi.llne2.View.Activity.Local.Original;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import net.mizofumi.llne2.BaasBox.API.BaasBoxAPI;
import net.mizofumi.llne2.BaasBox.Bean.Original.OriginalStampBean;
import net.mizofumi.llne2.R;
import net.mizofumi.llne2.View.Adapter.Local.Original.OriginalStampAdapter;

import java.util.List;

public class OriginalDetailActivity extends AppCompatActivity {

    FloatingActionButton fab;
    TextView name;
    ImageView topImage;
    String groupName;
    String groupId;
    String topImageUri;
    GridView gridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_original_detail);

        fab = (FloatingActionButton)findViewById(R.id.fab);
        name = (TextView)findViewById(R.id.name);
        groupName = getIntent().getStringExtra("groupName");
        name.setText(groupName);
        groupId = getIntent().getStringExtra("groupId");
        topImageUri = getIntent().getStringExtra("topImageUri");
        gridView = ((GridView)findViewById(R.id.stamps));
        topImage = (ImageView)findViewById(R.id.topImage);

        Picasso.with(OriginalDetailActivity.this).load(topImageUri).into(topImage);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OriginalDetailActivity.this,OriginalUploadActivity.class);
                intent.putExtra("groupName",groupName);
                intent.putExtra("groupId",groupId);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        final OriginalStampAdapter adapter = new OriginalStampAdapter(this);
        gridView.setAdapter(adapter);

        new BaasBoxAPI().getOriginStamps(groupId, new BaasBoxAPI.OriginalStampReciveListener() {
            @Override
            public void onListen(List<OriginalStampBean> list) {
                for (OriginalStampBean stamp : list) {
                    adapter.add(stamp);
                }
                Toast.makeText(OriginalDetailActivity.this, list.size()+"//", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError() {

            }
        });
    }
}
