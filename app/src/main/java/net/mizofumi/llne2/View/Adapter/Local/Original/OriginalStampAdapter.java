package net.mizofumi.llne2.View.Adapter.Local.Original;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.mizofumi.llne2.BaasBox.Bean.Original.OriginalStampBean;
import net.mizofumi.llne2.R;
import net.mizofumi.llne2.View.Activity.Local.Original.OriginalStampSendActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mizofumi on 2017/06/17.
 */

public class OriginalStampAdapter extends BaseAdapter {
    private OriginalStampAdapter.ViewHolder viewHolder;
    List<OriginalStampBean> list = new ArrayList<>();
    Context context;
    LayoutInflater inflater;

    public OriginalStampAdapter(Context context) {
        this.context = context;
        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null){
            convertView = inflater.inflate(R.layout.originalstamp_row,parent,false);
            viewHolder = new OriginalStampAdapter.ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (OriginalStampAdapter.ViewHolder)convertView.getTag();
        }

        viewHolder.name.setText(list.get(position).getName());
        Picasso.with(context).load(list.get(position).getImageUri()).into(viewHolder.stampImageView);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, OriginalStampSendActivity.class);
                intent.putExtra("id",list.get(position).getId());
                intent.putExtra("imageUri",list.get(position).getImageUri());
                context.startActivity(intent);
            }
        });
        return convertView;
    }

    public void add(OriginalStampBean stamp) {
        list.add(stamp);
        notifyDataSetChanged();
    }

    private class ViewHolder {
        TextView name;
        ImageView stampImageView;
        public ViewHolder(View view) {
            name = (TextView)view.findViewById(R.id.name);
            stampImageView = (ImageView)view.findViewById(R.id.stampImageView);
        }
    }
}
