package net.mizofumi.llne2.LlNE2;

import net.mizofumi.llne2.LlNE2.API.LlNE2API;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mizofumi on 2017/06/11.
 */

public class Sticker {

    private String source;
    private String title;
    private String preview;
    private String detail;
    private LlNE2API.CREATE_TYPE createType;
    private List<String> images = new ArrayList<>();

    public String getSource() {
        return source;
    }

    public Sticker setSource(String source) {
        this.source = source;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public Sticker setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getPreview() {
        return preview;
    }

    public Sticker setPreview(String preview) {
        this.preview = preview;
        return this;
    }

    public LlNE2API.CREATE_TYPE getCreateType() {
        return createType;
    }

    public Sticker setCreateType(LlNE2API.CREATE_TYPE createType) {
        this.createType = createType;
        return this;
    }

    public String getDetail() {
        return detail;
    }

    public Sticker setDetail(String detail) {
        this.detail = detail;
        return this;
    }

    public void add(String image){
        images.add(image);
    }

    public List<String> getImages() {
        return images;
    }
}
