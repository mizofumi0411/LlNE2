package net.mizofumi.llne2.BaasBox.API;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.baasbox.android.BaasDocument;
import com.baasbox.android.BaasFile;
import com.baasbox.android.BaasHandler;
import com.baasbox.android.BaasQuery;
import com.baasbox.android.BaasResult;
import com.baasbox.android.json.JsonObject;

import net.mizofumi.llne2.BaasBox.Bean.Original.OriginalStampBean;
import net.mizofumi.llne2.BaasBox.Bean.LlNE2.StampBean;
import net.mizofumi.llne2.BaasBox.Bean.Original.OriginGroupBean;
import net.mizofumi.llne2.BaasBox.Bean.LlNE2.StickerBean;
import net.mizofumi.llne2.Slack.API.SlackFilePost;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mizofumi on 2017/06/10.
 */

public class BaasBoxAPI {

    public void getStickers(String userId, final StickerListReciveListener listReciveListener){
        final List<StickerBean> stickerBeenList = new ArrayList<>();
        BaasQuery paginate = BaasQuery.builder().collection("Stickers").where("userId = ?").whereParams(userId).build();
        paginate.query(new BaasHandler<List<JsonObject>>() {
            @Override
            public void handle(BaasResult<List<JsonObject>> baasResult) {
                if (baasResult.isSuccess()){
                    List<JsonObject> list = baasResult.value();
                    Log.d("getStickers", String.valueOf(list.size()));
                    for (JsonObject j : list) {
                        stickerBeenList.add(new StickerBean(
                                j.getString("id"),
                                j.getString("userId"),
                                j.getString("name"),
                                j.getString("detail"),
                                j.getString("sourceUrl"),
                                j.getString("topImage"),
                                j.getFloat("rating")
                        ));
                    }
                    listReciveListener.onListen(stickerBeenList);
                }else {
                    listReciveListener.onError();
                }
            }
        });
    }

    public void getStamps(String stickerId,final StampListReciveListener listReciveListener){
        final List<StampBean> stampBeanList = new ArrayList<>();
        BaasQuery.Criteria c = BaasQuery.builder().where("attachedData.stickerId = ?").whereParams(stickerId).criteria();
        BaasFile.fetchAll(c, new BaasHandler<List<BaasFile>>() {
            @Override
            public void handle(BaasResult<List<BaasFile>> baasResult) {
                if (baasResult.isSuccess()){
                    for (BaasFile baasFile : baasResult.value()) {
                        stampBeanList.add(new StampBean(
                                baasFile.getId(),
                                baasFile.getId(),
                                baasFile.getAttachedData().getString("userId"),
                                baasFile.getAttachedData().getString("stickerId"),
                                baasFile.getStreamUri().toString(),
                                baasFile.getAttachedData().getString("sourceImageUrl")
                                ));
                    }
                    listReciveListener.onListen(stampBeanList);
                }else {
                    listReciveListener.onError();
                }
            }
        });
    }

    public void getOriginGroups(String userId, boolean isPrivate, final OriginGroupListReciveListener listener){
        final List<OriginGroupBean> originGroupBeanList = new ArrayList<>();
        BaasQuery paginate;
        if (userId != null && !userId.isEmpty() && isPrivate){
            paginate = BaasQuery.builder().collection("OriginGroups").where("author = ? and private = ?").whereParams(userId,isPrivate).orderBy("_creation_date desc").build();
        }else {
            paginate = BaasQuery.builder().collection("OriginGroups").where("private = ?").whereParams(isPrivate).orderBy("_creation_date desc").build();
        }
        paginate.query(new BaasHandler<List<JsonObject>>() {
            @Override
            public void handle(BaasResult<List<JsonObject>> baasResult) {
                if (baasResult.isSuccess()){
                    List<JsonObject> list = baasResult.value();
                    Log.d("getOriginGroups", String.valueOf(list.size()));
                    for (JsonObject j : list) {
                        originGroupBeanList.add(new OriginGroupBean(
                                j.getString("id"),
                                j.getString("_creation_date"),
                                j.getString("groupName"),
                                j.getString("author"),
                                j.getString("topImageId"),
                                j.getString("topImageUri"),
                                j.getBoolean("private")
                        ));
                    }
                    listener.onListen(originGroupBeanList);
                }else {
                    listener.onError();
                }
            }
        });
    }

    public void getOriginStamps(String groupId, final OriginalStampReciveListener listener){
        final List<OriginalStampBean> stampBeanList = new ArrayList<>();
        BaasQuery.Criteria c = BaasQuery.builder().where("attachedData.groupId = ?").whereParams(groupId).criteria();
        BaasFile.fetchAll(c, new BaasHandler<List<BaasFile>>() {
            @Override
            public void handle(BaasResult<List<BaasFile>> baasResult) {
                if (baasResult.isSuccess()){
                    for (BaasFile baasFile : baasResult.value()) {
                        stampBeanList.add(new OriginalStampBean(
                                baasFile.getId(),
                                baasFile.getAttachedData().getString("userId"),
                                baasFile.getAttachedData().getString("stampName"),
                                baasFile.getAttachedData().getString("groupId"),
                                baasFile.getStreamUri().toString()
                        ));
                    }
                    listener.onListen(stampBeanList);
                }else {
                    listener.onError();
                }
            }
        });
    }
    public interface OriginalStampReciveListener{
        void onListen(List<OriginalStampBean> list);
        void onError();
    }

    public void sendOriginGroup(String groupName, boolean isPrivate, String userId, BaasHandler<BaasDocument> handler){
        BaasDocument baasDocument = new BaasDocument("OriginGroups");
        baasDocument.put("groupName",groupName);
        baasDocument.put("private",isPrivate);
        baasDocument.put("author",userId);
        baasDocument.save(handler);
    }

    public void getOriginalTopImage(String groupId,BaasHandler<List<BaasFile>> handler) {
        BaasQuery.Criteria c = BaasQuery.builder().where("attachedData.groupId = ?").whereParams(groupId).orderBy("_creation_date desc").criteria();
        BaasFile.fetchAll(c, handler);
    }

    public void getStickerTopImage(String stickerId,BaasHandler<List<BaasFile>> handler){
        BaasQuery.Criteria c = BaasQuery.builder().where("attachedData.stickerId = ?").whereParams(stickerId).orderBy("_creation_date desc").criteria();
        BaasFile.fetchAll(c,handler);
//        BaasFile.fetchAll(c, new BaasHandler<List<BaasFile>>() {
//            @Override
//            public void handle(BaasResult<List<BaasFile>> baasResult) {
//                if (baasResult.isSuccess()){
//                    for (BaasFile baasFile : baasResult.value()) {
//                        fileBeanList.add(new StampBean(
//                                baasFile.getId(),
//                                baasFile.getId(),
//                                baasFile.getAttachedData().getString("userId"),
//                                baasFile.getAttachedData().getString("stickerId"),
//                                baasFile.getStreamUri().toString(),
//                                baasFile.getAttachedData().getString("sourceImageUrl")
//                        ));
//                    }
//                    listReciveListener.onListen(fileBeanList);
//                }else {
//                    listReciveListener.onError();
//                }
//            }
//        });
    }

    public void sendOriginFile(final JsonObject attachedData, final String groupId, final byte[] bytes,final SendOriginFileResultHandler handler){
        new Thread(new Runnable() {
            @Override
            public void run() {
                BaasFile file = new BaasFile(attachedData);
                file.upload(bytes, new BaasHandler<BaasFile>() {
                    @Override
                    public void handle(final BaasResult<BaasFile> imageResult) {
                        if (imageResult.isSuccess()){
                            BaasQuery paginate = BaasQuery.builder().collection("OriginGroups").where("id = ?").whereParams(groupId).build();
                            paginate.query(new BaasHandler<List<JsonObject>>() {
                                @Override
                                public void handle(BaasResult<List<JsonObject>> baasResult) {
                                    if (baasResult.isSuccess()){
                                        List<JsonObject> list = baasResult.value();
                                        for (JsonObject j : list) {
                                            j.put("topImageId",imageResult.value().getId());
                                            j.put("topImageUri",imageResult.value().getStreamUri().toString());
                                            BaasDocument document = BaasDocument.from(j);
                                            document.save(new BaasHandler<BaasDocument>() {
                                                @Override
                                                public void handle(final BaasResult<BaasDocument> baasResult) {
                                                    if (baasResult.isSuccess()){
                                                        new Handler().post(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                if (handler != null)
                                                                    handler.success();
                                                            }
                                                        });
                                                    }else {
                                                        error(baasResult.error().getMessage());
                                                    }
                                                }
                                            });
                                        }
                                    }else {
                                        error(baasResult.error().getMessage());
                                    }
                                }
                            });
                        }else {
                            error(imageResult.error().getMessage());
                        }
                    }

                    void error(final String message){
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                if (handler != null)
                                    handler.failed(message);
                            }
                        });
                    }
                });
            }
        }).start();
    }

    public void sendOriginFile(JsonObject attachedData, String groupId, Bitmap bitmap,SendOriginFileResultHandler handler){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100,byteArrayOutputStream);
        final byte[] bytes = byteArrayOutputStream.toByteArray();
        sendOriginFile(attachedData,groupId,bytes,handler);
    }

    public interface SendOriginFileResultHandler{
        void success();
        void failed(String errorMessage);
    }


    public interface StickerListReciveListener{
        void onListen(List<StickerBean> list);
        void onError();
    }

    public interface StampListReciveListener{
        void onListen(List<StampBean> list);
        void onError();
    }

    public interface OriginGroupListReciveListener{
        void onListen(List<OriginGroupBean> list);
        void onError();
    }

    public interface StampImageReciveListener{
        void onListen(Bitmap bitmap);
        void onError();
    }
}
